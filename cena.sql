--PROGRAMY=O
--POWIAZANIA=przelicz_jm
--POWIAZANE_TABELE=cs,cz

-- DROP FUNCTION IF EXISTS cena(boolean, integer, integer, integer, integer);
CREATE OR REPLACE FUNCTION cena(sprzedaz boolean, magazyn integer, cennik integer, towar integer, jm integer)
  RETURNS numeric AS
$BODY$DECLARE
  res numeric(20,4);
  cena text;
  rc double precision;
  rczl numeric(20,4);
  jmcen integer;
BEGIN
  res := 0.0;
  IF true AND sprzedaz THEN
    SELECT cs.scena,cs.rcena,cs.rcenazl,tow.ljmsprzedaz INTO cena,rc,rczl,jmcen FROM cs LEFT JOIN tow ON tow._id=cs.ltowar
      WHERE (cs._sect::integer=magazyn) AND (cs.lgrupa=cennik) AND (cs.ltowar=towar);
  ELSE
    SELECT cz.scena,cz.rcena,cz.rcenazl,tow.ljmzakup INTO cena,rc,rczl,jmcen FROM cz LEFT JOIN tow ON tow._id=cz.ltowar
      WHERE (cz._sect::integer=magazyn) AND (cz.lgrupa=cennik) AND (cz.ltowar=towar);
  END IF;

  IF FOUND THEN
    cena := trim(both ' ' from replace(upper(cena), 'CENA', rczl::text));
    IF cena='' THEN cena := '0'; END IF;
    EXECUTE 'SELECT (' || cena || ')::numeric(20,4)' INTO res;
    res := res * przelicz_jm(jm, jmcen, 1);
  END IF;
  RETURN res;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- ALTER FUNCTION cena(boolean, integer, integer, integer, integer) OWNER TO varicouser;
