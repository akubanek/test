-- dopisek nr 4

-- dopisek nr 3

-- DROP FUNCTION IF EXISTS zel(numeric);
CREATE OR REPLACE FUNCTION zel(numeric)
  RETURNS numeric AS
'Select CASE WHEN $1 is null THEN 0 ELSE CAST($1 as numeric) END;'
  LANGUAGE 'sql' IMMUTABLE
  COST 100;
-- ALTER FUNCTION zel(numeric) OWNER TO "varicouser";

-- DROP FUNCTION IF EXISTS zel(double precision);
CREATE OR REPLACE FUNCTION zel(double precision)
  RETURNS double precision AS
'Select CASE WHEN $1 is null THEN 0 ELSE CAST($1 as double precision) END;'
  LANGUAGE 'sql' IMMUTABLE
  COST 100;
-- ALTER FUNCTION zel(double precision) OWNER TO "varicouser";

