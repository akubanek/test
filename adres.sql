
-- DROP FUNCTION adres(character varying, character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION adres(kod character varying, poczta character varying, miejscowosc character varying, ulica character varying, nr_domu character varying, nr_mieszk character varying)
  RETURNS character varying AS
$BODY$DECLARE
  res character varying;
BEGIN
  res := '';
  IF (kod <> '') THEN
    res := res || ' ' || kod;
    IF (poczta <> '') THEN
      res := res || ' ' || poczta;
    END IF;
    res := res || ',';
  END IF;

  IF (miejscowosc <> '') AND (lower(miejscowosc) <> lower(poczta)) THEN
    res := res || ' ' || miejscowosc;
  END IF;

  --komentarz cztery
  IF (nr_domu <> '') THEN
    res := res || ' ' || nr_domu;
    IF (nr_mieszk <> '') THEN
      res := res || '/' || nr_mieszk;
    END IF;
  END IF;
  res := trim(trailing ',' from res);
  res := trim(leading ' ' from res);
  RETURN res;
END;
$BODY$
  LANGUAGE 'plpgsql' IMMUTABLE
  COST 100;
-- ALTER FUNCTION adres(character varying, character varying, character varying, character varying, character varying, character varying) OWNER TO varicouser;
