--dopisek nr 4
--POWIAZANIA=

-- DROP FUNCTION IF EXISTS val(numeric);
CREATE OR REPLACE FUNCTION val(numeric)
  RETURNS numeric AS
'Select CASE WHEN $1 is null THEN 0 ELSE CAST($1 as numeric) END;'
  LANGUAGE 'sql' IMMUTABLE
  COST 100;
-- ALTER FUNCTION val(numeric) OWNER TO "varicouser";

-- DROP FUNCTION IF EXISTS val(double precision);
CREATE OR REPLACE FUNCTION val(double precision)
  RETURNS double precision AS
'Select CASE WHEN $1 is null THEN 0 ELSE CAST($1 as double precision) END;'
  LANGUAGE 'sql' IMMUTABLE
  COST 100;
-- ALTER FUNCTION val(double precision) OWNER TO "varicouser";

